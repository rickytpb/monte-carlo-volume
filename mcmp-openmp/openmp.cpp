//
//  main.cpp
//  mcmp
//
//  Created by Jacek Smoliński on 14/04/2021.
//

#include <cstdio>
#include <random>
#include <cmath>
#include <omp.h>

int check_point(long double &x1, long double &x2, long double &x3){
    if( (pow(x1,2)+pow(x3,2)) > 7.)
        return 0;
    if((pow(x1,2) + pow(x2,2) + pow(x3,2)) > 12.)
        return 0;
    return 1;
}

int get_point_inside(std::uniform_real_distribution<long double> &distr, std::mt19937 &gen){
    long double x1,x2,x3;
    x1 = distr(gen);
    x2 = distr(gen);
    x3 = distr(gen);
    return check_point(x1,x2,x3);
};

int main(int argc, const char * argv[]) {
    long double local;
    long double procent=0.;
    int all=170000;
    int iter = 100;
    int inside;

    //Zgodnie z sugestią zamieniłem sekcje krytyczną na reduction
    //Po wynikach wydaje się ze dziala dobrze
    #pragma omp paralell private(local,inside) reduction(+ : procent)
    {
    std::random_device rd;
    std::mt19937 gen(rd()*omp_get_num_threads());
    std::uniform_real_distribution<long double> distr(0., std::nextafter(sqrt(12.), std::numeric_limits<long double>::max()));
    #pragma omp for 
            for(int i=0; i<iter; ++i){
                inside = 0;
                for(int n=0; n<all; ++n)
                {
                    inside += get_point_inside(distr, gen);
                };
                local = inside/(long double)all;
                {procent += local;}
       }
    }
    procent /= (long double)iter;
    printf("%.8Lf\n", procent * pow(sqrt(12.),3.));
    
    return 0;
}
