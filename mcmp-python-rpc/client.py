import threading
import xmlrpc.client

def get_partial_result(iter_n, results, i):
    with xmlrpc.client.ServerProxy("http://localhost:8123/") as proxy:
        results[i] = float(proxy.get_mc_vol(iter_n))

threadNum = 8
partialResults = [None]*threadNum
threadList = []
for i in range(threadNum):
    t= threading.Thread(target=get_partial_result, args = (10000, partialResults, i ))
    t.start()
    threadList.append(t)

for thread in threadList:
    thread.join()

print(sum(partialResults)/threadNum)