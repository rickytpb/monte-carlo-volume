import random
import math
from xmlrpc.server import SimpleXMLRPCServer

def get_x():
    return random.uniform(0.,math.sqrt(12.))

def is_inside(x1,x2,x3):
    if math.pow(x1,2) + math.pow(x2,2) + math.pow(x3,2) > 12.:
        return 0
    if math.pow(x1,2) + math.pow(x3,2) > 7.:
        return 0
    return 1

def get_mc_vol(iters):
    inside = 0
    for i in range(iters):
        x1 = get_x()
        x2 = get_x()
        x3 = get_x()
        inside += is_inside(x1,x2,x3)
    return inside/float(iters) * math.pow(math.sqrt(12),3)


server = SimpleXMLRPCServer(("localhost", 8123))
server.register_function(get_mc_vol, "get_mc_vol")
server.serve_forever()