//
//  main.cpp
//  mcmp
//
//  Created by Jacek Smoliński on 14/04/2021.
//

#include <cstdio>
#include <random>
#include <cmath>

int check_point(long double &x1, long double &x2, long double &x3){
    if( (pow(x1,2)+pow(x3,2)) > 7.)
        return 0;
    if((pow(x1,2) + pow(x2,2) + pow(x3,2)) > 12.)
        return 0;
    return 1;
}

int get_point_inside(std::uniform_real_distribution<long double> &distr, std::mt19937 &gen){
    long double x1,x2,x3;
    x1 = distr(gen);
    x2 = distr(gen);
    x3 = distr(gen);
    return check_point(x1,x2,x3);
};

int main(int argc, const char * argv[]) {
    int inside = 0, all = 12200000;
    std::random_device rd; // obtain a random number from hardware
    std::mt19937 gen(rd()); // seed the generator
    std::uniform_real_distribution<long double> distr(0, std::nextafter(sqrt(12.), std::numeric_limits<long double>::max())); // define the range
    for(int i=0; i<all; ++i)
    {
        inside += get_point_inside(distr, gen);
    };
    
    printf("%.8Lf\n", (inside/(long double)all) * pow(sqrt(12.),3.));
    
    return 0;
}
