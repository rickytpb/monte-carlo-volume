//
//  openmpi.cpp
//  mcmp
//
//  Created by Jacek Smoliński on 21/06/2021.
//

#include <stdio.h>
#include <random>
#include <cmath>
#include <mpi.h>

#define MY_TAG 1

int check_point(long double &x1, long double &x2, long double &x3){
    if( (pow(x1,2)+pow(x3,2)) > 7.)
        return 0;
    if((pow(x1,2) + pow(x2,2) + pow(x3,2)) > 12.)
        return 0;
    return 1;
}

int get_point_inside(std::uniform_real_distribution<long double> &distr, std::mt19937 &gen){
    long double x1,x2,x3;
    x1 = distr(gen);
    x2 = distr(gen);
    x3 = distr(gen);
    return check_point(x1,x2,x3);
};

long double get_partial_result(int iters, int seed)
{
    int inside=0;
    long double local;
    std::random_device rd;
    std::mt19937 gen(rd()*seed);
    std::uniform_real_distribution<long double> distr(0., std::nextafter(sqrt(12.), std::numeric_limits<long double>::max()));
    for(int n=0; n<iters; ++n)
    {
        inside += get_point_inside(distr, gen);
    };
    local = inside/(long double)iters;
    return local;
}

int main(int argc, char** argv) {
    int rank, size;
        MPI_Init (&argc, &argv);
        MPI_Comm_rank (MPI_COMM_WORLD, &rank);
        MPI_Comm_size (MPI_COMM_WORLD, &size);
    long double local_sum = get_partial_result(11000000,rank);
    long double global_sum;
    MPI_Reduce(&local_sum, &global_sum, 1, MPI_LONG_DOUBLE, MPI_SUM, 0,
               MPI_COMM_WORLD);
        if (rank == 0) {
            printf("%Lf\n",(global_sum/(double)size) * pow(sqrt(12.),3.));
    }
        MPI_Finalize();
    return 0;
}
