//
//  thread.cpp
//  mcmp
//
//  Created by Jacek Smoliński on 14/04/2021.
//

#include <cstdio>
#include <random>
#include <cmath>
#include <thread>
#include <mutex>
#include <vector>

const int tn = 64;
std::mutex mut;

int check_point(long double &x1, long double &x2, long double &x3){
    if( (pow(x1,2)+pow(x3,2)) > 7.)
        return 0;
    if((pow(x1,2) + pow(x2,2) + pow(x3,2)) > 12.)
        return 0;
    return 1;
}

int get_point_inside(std::uniform_real_distribution<long double> &distr, std::mt19937 &gen){
    long double x1,x2,x3;
    x1 = distr(gen);
    x2 = distr(gen);
    x3 = distr(gen);
    return check_point(x1,x2,x3);
};

void get_partial_result(int iters, int seed, long double &result)
{
    int inside=0;
    long double local;
    std::random_device rd;
    std::mt19937 gen(rd()*seed);
    std::uniform_real_distribution<long double> distr(0., std::nextafter(sqrt(12.), std::numeric_limits<long double>::max()));
    for(int n=0; n<iters; ++n)
    {
        inside += get_point_inside(distr, gen);
    };
    local = inside/(long double)iters;
    const std::lock_guard<std::mutex> lock(mut);
    result += local;
}

int main(int argc, const char * argv[]) {
    long double procent=0.;
    int base_iterations = 1240000;
    
    std::vector<std::thread> trds;
    std::vector<std::thread> htrds;
    for (int j=0;j<tn;++j)
        {
            trds.push_back(std::thread {get_partial_result,base_iterations, j, std::ref(procent)});
        }
        for(std::thread &trd : trds) trd.join();

        procent /= (long double)tn;

    printf("%.8Lf\n", procent * pow(sqrt(12.),3.));
    
    return 0;
}
